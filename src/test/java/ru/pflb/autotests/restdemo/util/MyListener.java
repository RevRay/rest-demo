package ru.pflb.autotests.restdemo.util;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class MyListener implements ITestListener {
    public void onTestStart(ITestResult result) {
        System.out.println("listener: started test");
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("listener: test passed");
    }

    public void onTestFailure(ITestResult result) {
        System.out.println("listener: test failed");
    }

    public void onTestSkipped(ITestResult result) {

    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    public void onStart(ITestContext context) {

    }

    public void onFinish(ITestContext context) {

    }
}
