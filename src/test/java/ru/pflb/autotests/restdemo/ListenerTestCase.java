package ru.pflb.autotests.restdemo;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.pflb.autotests.restdemo.util.MyListener;

@Listeners(MyListener.class)
public class ListenerTestCase {
    @Test
    public void test(){
        System.out.println("pass");
    }
    @Test
    public void testFail(){
        System.out.println("fail");
        throw new RuntimeException();
    }
}
