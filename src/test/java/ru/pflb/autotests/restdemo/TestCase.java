package ru.pflb.autotests.restdemo;

import io.restassured.path.json.JsonPath;
import org.testng.Assert;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class TestCase {
    @Test
    public void test(){
        String url = "https://api.iextrading.com/1.0/stock/aapl/company";
        given()
        .when().get(url)
        .then().statusCode(200);
    }
    @Test
    public void testData1(){
        String url = "https://reqres.in/api/users";
        String body = given().queryParam("delay", "3")
        .when().get(url)
        .then().extract().body().asString();
        System.out.println(body);

    }

    @Test
    public void testData(){
        String url = "https://api.iextrading.com/1.0/stock/aapl/company";
        JsonPath body = given()
        .when().get(url)
        .then().extract().body().jsonPath();

        String symbol = body.get("symbol");
        Assert.assertEquals(symbol, "AAPL");
    }
}
